﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pipe
{
    public Transform pipeHeadTransform { get; set; }
    public Transform pipeBodyTransform { get; set; }
    public bool isBottom { get; set; }

    public const float PIPE_MOVE_SPEED = 30f;

    public Pipe(Transform pipeHeadTransform, Transform pipeBodyTransform, bool isBottom)
    {
        this.pipeHeadTransform = pipeHeadTransform;
        this.pipeBodyTransform = pipeBodyTransform;
        this.isBottom = isBottom;
    }

    public void Move()
    {
        pipeHeadTransform.position += new Vector3(-1, 0, 0) * PIPE_MOVE_SPEED * Time.deltaTime;
        pipeBodyTransform.position += new Vector3(-1, 0, 0) * PIPE_MOVE_SPEED * Time.deltaTime;
    }

    public float GetXPosition()
    {
        return pipeHeadTransform.position.x;
    }

    public bool IsBottom()
    {
        return isBottom;
    }

}
